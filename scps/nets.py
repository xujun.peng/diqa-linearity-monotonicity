import warnings

with warnings.catch_warnings():
    from keras.layers.convolutional import Conv2D, MaxPooling2D, AveragePooling2D
    from keras.layers import Input, Activation, LeakyReLU, concatenate, Add
    from keras.layers.core import Dense, Flatten, Dropout
    from keras.layers.merge import add
    from keras.layers.normalization import BatchNormalization
    from keras.models import Model
    from keras.regularizers import l2


class SiameseDIQAModel:
    def _bn_relu(self, input):
        """Helper to build a BN -> relu block
        """
        norm = BatchNormalization(axis=-1)(input)
        return LeakyReLU(alpha=0.3)(norm)

    def _conv_bn_relu(self, **conv_params):
        """Helper to build a conv -> BN -> relu block
        """
        name = conv_params["name"]
        filters = conv_params["filters"]
        strides = conv_params.setdefault("strides", 1)
        padding = conv_params.setdefault("padding", "same")
        kernel_size = conv_params["kernel_size"]
        kernel_initializer = conv_params.setdefault("kernel_initializer", "he_normal")
        kernel_regularizer = conv_params.setdefault("kernel_regularizer", l2(1.0e-4))

        def f(input):
            conv = Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                strides=strides,
                padding=padding,
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
                name=name,
            )(input)
            return self._bn_relu(conv)

        return f

    def build_model(self, input_size=128, **params):
        def resnet(input_size):
            input_img = Input(shape=(input_size, input_size, 1), name="resnet_input")

            # resnet 1
            x = self._conv_bn_relu(filters=32, kernel_size=(7, 7), name="conv1")(
                input_img
            )
            b = self._conv_bn_relu(filters=16, kernel_size=(1, 1), name="res1_conv1")(x)
            b = self._conv_bn_relu(filters=16, kernel_size=(3, 3), name="res1_conv2")(b)
            b = self._conv_bn_relu(filters=32, kernel_size=(1, 1), name="res1_conv3")(b)
            m = LeakyReLU(alpha=0.2)(add([x, b]))

            # resnet 2
            x = MaxPooling2D((2, 2), padding="same")(m)  # 64x64
            x = self._conv_bn_relu(filters=64, kernel_size=(3, 3), name="conv2")(x)
            b = self._conv_bn_relu(filters=32, kernel_size=(1, 1), name="res2_conv1")(x)
            b = self._conv_bn_relu(filters=32, kernel_size=(3, 3), name="res2_conv2")(b)
            b = self._conv_bn_relu(filters=64, kernel_size=(1, 1), name="res2_conv3")(b)
            m = LeakyReLU(alpha=0.2)(add([x, b]))

            # resnet 3
            x = MaxPooling2D((2, 2), padding="same")(m)  # 32x32
            x = self._conv_bn_relu(filters=128, kernel_size=(3, 3), name="conv3")(x)
            b = self._conv_bn_relu(filters=64, kernel_size=(1, 1), name="res3_conv1")(x)
            b = self._conv_bn_relu(filters=64, kernel_size=(3, 3), name="res3_conv2")(b)
            b = self._conv_bn_relu(filters=128, kernel_size=(1, 1), name="res3_conv3")(
                b
            )
            m = LeakyReLU(alpha=0.2)(add([x, b]))

            # resnet 4
            x = MaxPooling2D((2, 2), padding="same")(m)  # 16x16
            x = self._conv_bn_relu(filters=256, kernel_size=(3, 3), name="conv4")(x)
            b = self._conv_bn_relu(filters=128, kernel_size=(1, 1), name="res4_conv1")(
                x
            )
            b = self._conv_bn_relu(filters=128, kernel_size=(3, 3), name="res4_conv2")(
                b
            )
            b = self._conv_bn_relu(filters=256, kernel_size=(1, 1), name="res4_conv3")(
                b
            )
            m = LeakyReLU(alpha=0.2)(add([x, b]))

            x = MaxPooling2D((2, 2), padding="same")(m) # 8x8
            x = Flatten()(x)

            return Model(inputs=input_img, outputs=x, name="resnet")

        input1 = Input(shape=(input_size, input_size, 1), name="input1")
        input2 = Input(shape=(input_size, input_size, 1), name="input2")

        # two branches
        shared_siamese = resnet(input_size)
        feature1 = shared_siamese(input1)
        feature2 = shared_siamese(input2)

        b1 = Dense(units=2048, activation="tanh")(feature1)
        b2 = Dense(units=2048, activation="tanh")(feature2)

        x = concatenate([b1, b2], axis=-1)
        x = Dense(units=1024, activation="relu")(x)
        x = Dropout(rate=0.3)(x)
        x = Dense(units=512, activation="relu")(x)
        x = Dropout(rate=0.3)(x)
        x = Dense(units=1, activation="sigmoid")(x)

        return Model(inputs=[input1, input2], outputs=x)
