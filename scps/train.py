import warnings

with warnings.catch_warnings():
    import os, sys
    import argparse
    import keras
    from keras.callbacks import ModelCheckpoint
    from datetime import datetime
    from etl import DIQASequenceMask
    from nets import SiameseDIQAModel
    from loss import ranking_loss


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = argparse.ArgumentParser(
        description="document image quality assessement training."
    )
    parser.add_argument(
        "-i", "--training-db", dest="train_db", help="The input training dataset"
    )
    parser.add_argument(
        "-v", "--validation-db", dest="validate_db", help="The input validation dataset"
    )
    parser.add_argument(
        "-s",
        "--image-size",
        type=int,
        default=128,
        dest="patch_size",
        help="The croped patches' dimension",
    )
    parser.add_argument(
        "-n",
        "--sample-num",
        type=int,
        dest="sample_num",
        help="The sampling number for training dataset",
    )
    parser.add_argument(
        "-b",
        "--batch-size",
        type=int,
        dest="batch_size",
        help="The batch size for  training",
    )
    parser.add_argument(
        "-l", "--learning-rate", type=float, dest="lr", help="The learning rate"
    )
    parser.add_argument(
        "-e", "--epochs", type=int, dest="epochs", help="Training epochs"
    )
    parser.add_argument(
        "-u",
        "--pairwise-loss-weight",
        type=float,
        dest="weight",
        help="pairwise loss weight",
    )
    parser.add_argument(
        "-m", "--input-model-path", dest="in_model", help="Input model path"
    )
    parser.add_argument(
        "-o", "--output-model-path", dest="out_model", help="Output model path"
    )
    args = parser.parse_args()

    print(datetime.now())

    print("Start loading data...")

    prepare_data = DIQASequenceMask

    val_db = prepare_data(
        args.validate_db, args.patch_size, args.batch_size, args.sample_num
    )
    val_x1, val_x2, val_y = val_db._prepare_data(val_db.db)

    print("Start build model...")
    model = SiameseDIQAModel().build_model(input_size=args.patch_size)

    # fine tune
    if os.path.isfile(args.in_model):
        print("Start fine tuning...")
        model.load_weights(args.in_model)

        adadelta = keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=1e-08, decay=0.0)
        model.compile(
            optimizer=adadelta,
            loss=ranking_loss(args.weight),
            metrics=["mse", "mae"],
        )
    # train new
    else:
        print("Start new training...")
        sgd = keras.optimizers.SGD(
            lr=args.lr, momentum=0.9, decay=1e-06, nesterov=True, clipnorm=5
        )
        model.compile(
            optimizer=sgd,
            loss=ranking_loss(args.weight),
            metrics=["mse", "mae"],
        )

    # start trainig
    checkpoint = ModelCheckpoint(
        args.out_model,
        monitor="val_mean_squared_error",
        verbose=1,
        save_best_only=True,
        mode="min",
    )
    callbacks_list = [checkpoint]

    model.fit_generator(
        prepare_data(args.train_db, args.patch_size, args.batch_size, args.sample_num),
        validation_data=([val_x1, val_x2], val_y),
        epochs=args.epochs,
        callbacks=callbacks_list,
        class_weight=None,
        use_multiprocessing=True,
        workers=4,
    )


if __name__ == "__main__":
    sys.exit(main())
